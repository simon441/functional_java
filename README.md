# Functional Programming in Java

Link to video: https://www.youtube.com/watch?v=rPSL1alFIjI&list=PLWKjhJtqVAbnRT_hue-3zyiuIYj0OlpyG

## Rules of functional programming:
Pure functional programming has a set of rules to follow
- No state
- Pure Functions
- No Side Effects
- Higher Order Functions
  + The function takes one or more functions as parameters.
  + The function returns another function as result.
