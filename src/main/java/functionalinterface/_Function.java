package functionalinterface;

import java.util.function.BiFunction;
import java.util.function.Function;

public class _Function {
    public static void main(String[] args) {

        // Function example: takes 1 argument and produces 1 result
        int increment = incrementByOne(1);
        System.out.println(increment);

        Integer incrementByOne = incrementByOneFunction.apply(1);
        System.out.println(incrementByOne);
        System.out.println(incrementByOneFunction.andThen(multiplyBy10).apply(1));
        System.out.println(incrementByOneFunction.andThen(multiplyBy10).apply(4));

        // Bi-Function example: takes 2 arguments and produces 1 result
        System.out.println(incrementByOneAndMultiply(4, 100));
        System.out.println(incrementByOneAndMultiplyBiFunction.apply(4, 100));
    }

    static Function<Integer, Integer> incrementByOneFunction =
            number -> number + 1;

    static Function<Integer, Integer> multiplyBy10 =
            number -> number * 10;

    static BiFunction<Integer, Integer, Integer> incrementByOneAndMultiplyBiFunction =
            (numberToIncrementBy, numberToMultiplyBy) ->
                    (numberToIncrementBy + 1) * numberToMultiplyBy;

    static int incrementByOneAndMultiply(int number, int numberToMultiplyBy) {
        return (number + 1) * numberToMultiplyBy;
    }


    static int incrementByOne(int number) {
        return number + 1;
    }

}
