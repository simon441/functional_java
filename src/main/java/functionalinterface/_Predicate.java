package functionalinterface;

import java.util.function.Predicate;

public class _Predicate {
    public static void main(String[] args) {
        System.out.println("Without Predicate");
        System.out.println(isPhoneNumberValid("07000000000"));
        System.out.println(isPhoneNumberValid("09000000000"));
        System.out.println(isPhoneNumberValid("0900000000"));

        System.out.println("With Predicate");
        System.out.println(isPhoneNumberValidPredicate.test("07000000000"));
        System.out.println(isPhoneNumberValidPredicate.test("09000000000"));
        System.out.println(isPhoneNumberValidPredicate.test("0900000000"));

        System.out.println("Chain Predicates");
        System.out.println("Is phone number valid and contains number 3: " + isPhoneNumberValidPredicate.and(containsNumberThree).test("07000030000"));
        System.out.println("Is phone number valid and contains number 3: " + isPhoneNumberValidPredicate.and(containsNumberThree).test("0000003000"));
        System.out.println("Is phone number valid or contains number 3: " + isPhoneNumberValidPredicate.or(containsNumberThree).test("07000000000"));
        System.out.println("Is phone number valid or contains number 3: " + isPhoneNumberValidPredicate.or(containsNumberThree).test("03000000"));
        System.out.println("Negate the condition: Is phone number valid or contains number 3: " + isPhoneNumberValidPredicate.or(containsNumberThree).negate().test("0900000000"));

    }

    static Predicate<String> isPhoneNumberValidPredicate = phoneNumber ->
            phoneNumber.startsWith("07") && phoneNumber.length() == 11;

    static Predicate<String> containsNumberThree = phoneNumber ->
            phoneNumber.contains("3");

    static boolean isPhoneNumberValid(String phoneNumber) {
        return phoneNumber.startsWith("07") && phoneNumber.length() == 11;
    }
}
