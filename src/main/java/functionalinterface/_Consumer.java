package functionalinterface;

import java.util.function.BiConsumer;
import java.util.function.Consumer;

public class _Consumer {
    public static void main(String[] args) {
        Customer maria = new Customer("Maria", "99999");

        // Normal Java function
        greetCustomer(maria);

        // Consumer Functional Interface
        greetCustomerConsumer.accept(maria);

        // Bi-Consumer
        greetCustomerv2(maria, true);
        greetCustomerv2(maria, false);
        greetCustomerConsumerV2.accept(maria, true);
        greetCustomerConsumerV2.accept(maria, false);
    }

    static BiConsumer<Customer, Boolean> greetCustomerConsumerV2 = (customer, showPhoneNumber) ->
            System.out.println(
                    "Hello " + customer.getCustomerName() + ",  thanks for registering phone number "
                            + (showPhoneNumber ? customer.getCustomerPhoneNumber() : "********")
            );


    static void greetCustomerv2(Customer customer, boolean showPhoneNumber) {
        System.out.println(
                "Hello " + customer.getCustomerName() + ",  thanks for registering phone number "
                        + (showPhoneNumber ? customer.getCustomerPhoneNumber() : "***************")
        );
    }

    static Consumer<Customer> greetCustomerConsumer = customer ->
            System.out.println("Hello " + customer.getCustomerName() + ",  thanks for registering phone number " + customer.getCustomerPhoneNumber());


    static void greetCustomer(Customer customer) {
        System.out.println("Hello " + customer.getCustomerName() + ",  thanks for registering phone number " + customer.getCustomerPhoneNumber());
    }

    static class Customer {
        private final String customerName;
        private final String customerPhoneNumber;

        Customer(String customerName, String customerPhone) {
            this.customerName = customerName;
            this.customerPhoneNumber = customerPhone;
        }

        public String getCustomerName() {
            return customerName;
        }

        public String getCustomerPhoneNumber() {
            return customerPhoneNumber;
        }
    }
}
