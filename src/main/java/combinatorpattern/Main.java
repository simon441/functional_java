package combinatorpattern;

import java.time.LocalDate;

import static combinatorpattern.CustomerRegistrationValidator.*;

public class Main {

    public static void main(String[] args) {
        var customer = new Customer(
                "Alice",
                "alice@domain.tld",
                "+089898999889",
                LocalDate.of(2000, 1, 1)
        );

//        System.out.println(new CustomerValidationService().isValid(customer));
//
//        // if valid store customer in db
//
        customer = new Customer(
                "Alice",
                "alicedomain.tld",
                "89898999889",
                LocalDate.of(2000, 1, 1)
        );
//        System.out.println(new CustomerValidationService().isValid(customer));

        // Using combinator
        final ValidationResult result = isEmailValid()
                .and(isPhoneNumberValid())
                .and(isAdult())
                .apply(customer);

        System.out.println(result);

        if (result != ValidationResult.SUCCESS) {
            throw new IllegalStateException(result.name());
        }

    }
}
