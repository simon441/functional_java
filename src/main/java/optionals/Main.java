package optionals;

import java.util.Optional;
import java.util.function.Supplier;

public class Main {
    public static void main(String[] args) {
        // prints default value
        Object value = Optional.ofNullable(null)
                .orElseGet(() -> "default value");
        System.out.println(value);
        // prints the email
        value = Optional.ofNullable("email@domain.tld")
                .orElseGet(() -> "default value");
        System.out.println(value);

        // user throw an exception if null
//        value = Optional.ofNullable("email@domain.tld")
//                .orElseThrow(() -> new IllegalStateException("exception"));
//        final Supplier<IllegalStateException> exceptionSupplier = () -> new IllegalStateException("exception");
//        value = Optional.ofNullable(null)
//                .orElseThrow(exceptionSupplier);

        // If Present
        Optional.ofNullable("john@domain.tld")
                .ifPresent(email -> System.out.println("Sending email to " + email));

        // If Present or Else
        Optional.ofNullable(null)
                .ifPresentOrElse(email -> System.out.println("Sending email to " + email),
                        () -> {
                            System.out.println("Cannot send email");
                        });

        Optional.ofNullable("john@domain.tld")
                .ifPresentOrElse(
                        email -> System.out.println("Sending email to " + email),
                        () -> System.out.println("Cannot send email"));
    }
}
