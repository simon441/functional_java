package streams;

import java.util.List;
import java.util.function.Function;
import java.util.function.IntConsumer;
import java.util.function.Predicate;
import java.util.function.ToIntFunction;
import java.util.stream.Collectors;

import static streams.Gender.*;

public class _Stream {
    public static void main(String[] args) {
        List<Person> people = List.of(
                new Person("John", MALE),
                new Person("Maria", FEMALE),
                new Person("Aisha", FEMALE),
                new Person("Alex", MALE),
                new Person("Alice", FEMALE),
                new Person("Bay", PREFER_NOT_TO_SAY)
        );

        // Get all genders
        people.stream()
                .map(person -> person.getGender())
                .collect(Collectors.toSet())
                .forEach(System.out::println);

        // Get names
        people.stream()
                .map(person -> person.getName())
                .forEach(System.out::println);

        // Count names
        people.stream()
                .map(person -> person.getName())
                .mapToInt(String::length) // name -> name.length()
                .forEach(System.out::println);

        final ToIntFunction<String> length = String::length;
        final Function<Person, String> personStringFunction = person -> person.getName();
        final IntConsumer println = x -> System.out.println(x);
        people.stream()
                .map(personStringFunction)
                .mapToInt(length)
                .forEach(println);


        // Match All
        final boolean containsOnlyFemales = people.stream()
                .allMatch(person -> FEMALE.equals(person.getGender()));

        final Predicate<Person> personPredicate = person -> FEMALE.equals(person.getGender());
        final boolean containsOnlyFemales2 = people.stream()
                .allMatch(personPredicate);

        System.out.println("Contains only Females: " + containsOnlyFemales);

        // Any Match
        final boolean mayContainFemales = people.stream()
                .anyMatch(person -> FEMALE.equals(person.getGender()));
        System.out.println("Contains Females: " + mayContainFemales);

    }
}
