package finalsection;

import java.util.Locale;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;

public class Lambdas {
    public static void main(String[] args) {
        Integer number = null;
        int counter = 0;

        Function<String, String> upperCaseName = name -> name.toUpperCase(Locale.ROOT);

        Function<String, String> upperCaseName2 = name -> {
            // logic
            if (name.isBlank()) throw new IllegalArgumentException("");
            return name.toUpperCase(Locale.ROOT);
        };

        BiFunction<String, Integer, String> upperCaseName3 = (name, age) -> {
            // logic
            if (name.isBlank()) throw new IllegalArgumentException("");
            System.out.println(age);
            return name.toUpperCase(Locale.ROOT);
        };

        System.out.println(upperCaseName.apply("Simon"));
        System.out.println(upperCaseName2.apply("Simon"));
        System.out.println(upperCaseName3.apply("Simon", 20));
    }
}
