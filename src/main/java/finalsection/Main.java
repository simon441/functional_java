package finalsection;

import java.util.function.Consumer;

public class Main {
    public static void main(String[] args) {

//        hello("John", "Doe", null);
        hello("John",
                null,
                value -> System.out.println("no last name provided for " + value));

        helloRunnable("John",
                null,
                () -> System.out.println("no last name provided"));
    }

    static void hello(String firstName, String lastName, Consumer<String> callback) {
        System.out.println(firstName);
        if (lastName != null) {
            System.out.println(lastName);
        } else {
            callback.accept(firstName);
        }
    }

    static void helloRunnable(String firstName, String lastName, Runnable callback) {
        System.out.println(firstName);
        if (lastName != null) {
            System.out.println(lastName);
        } else {
            callback.run();
        }
    }

    /*
     * javascript callback example:
     * function hello(firstName, lastName, callback) {
     *     console.log(firstName);
     *     if (lastName) {
     *         console.log(lastName);
     *     } else {
     *         callback();
     *     }
     * }
     */
}
